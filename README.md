# Developing notes
Реалізовано: 
* Crud для сутності Connection.
* Реалізовано FTP client (оснований на цьому прикладі: https://github.com/author135135/django-ftp-client)
* усе, що було зазначено в project specification, з одним "але". Замість SFTP використано FTP.
Робота над SFTP-прослойкою ***in progress***. 

# Deploying info
https://stark-sierra-59649.herokuapp.com/ - працюючий екземпляр додатку. Враховуючи особливості
сервісу Heroku, якщо певний час ніхто не заходив в додаток, то instance, на якому він крутиться
"засипає" і тому перший старт може тривати 15 секунд +-.

Для перевірки роботи FTP клієнта рекомендую скористатись публічним READ Only FTP server`ом: demo.wftpserver.com,
що вже збережений в списку connections'ів під №2. Credentials взято з: https://www.wftpserver.com/onlinedemo.htm
Також можете скористатись будь-яким бажаним вами FTP-server'ом.