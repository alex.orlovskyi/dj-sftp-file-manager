from django.urls import path
from . import views

app_name = 'core'
urlpatterns = [
    path('', views.SFTPConnectionList.as_view(), name='sftpconnection_list'),
    path('view/<int:pk>', views.SFTPConnectionDetail.as_view(), name='sftpconnection_detail'),
    path('new', views.SFTPConnectionCreate.as_view(), name='sftpconnection_new'),
    path('edit/<int:pk>', views.SFTPConnectionUpdate.as_view(), name='sftpconnection_edit'),
    path('delete/<int:pk>', views.SFTPConnectionDelete.as_view(), name='sftpconnection_delete'),
    path('connect/<int:pk>/', views.SFTPConnectionConnect.as_view(), name='sftpconnection_connect'),
]
